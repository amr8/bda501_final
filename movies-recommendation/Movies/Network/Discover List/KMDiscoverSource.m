//
//  KMDiscoverMapSource.m
//  TheMovieDB
//
//  Created by Kevin Mindeguia on 03/02/2014.
//  Copyright (c) 2014 iKode Ltd. All rights reserved.
//

#import "KMDiscoverSource.h"
#import "KMSourceConfig.h"
#import "AFNetworking.h"
#import "KMMovie.h"

#define kSimilarMoviesUrlFormat @"%@?apikey=%@&t=%@"

@implementation KMDiscoverSource

#pragma mark - Init Methods

+ (KMDiscoverSource *)discoverSource;
{
    static dispatch_once_t onceToken;
    static KMDiscoverSource* instance = nil;

    dispatch_once(&onceToken, ^{
        instance = [[KMDiscoverSource alloc] init];
    });
    return instance;
}

#pragma mark - Request Methods

- (void)getDiscoverList:(NSString *)pageLimit completion:(KMDiscoverListCompletionBlock)completionBlock;
{
    NSDictionary* parameters = @{ @"query": @{ @"match": @{@"year":@"2008"} },
                                  @"_source": @[@"id", @"title",@"year",@"genre"],
                                  @"sort" : @[@{ @"_id" : @"asc" }],
                                  @"size":@"10"
                                  };

    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    [manager POST:[self prepareUrlElasticSearch] parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         NSArray *resArr = [[responseObject objectForKey:@"hits"] objectForKey:@"hits"];

         __block NSMutableArray *newData = [[NSMutableArray alloc] init];
         for (__block int i=0; i< [resArr count]; i++) {
             NSString *movieName = [NSString stringWithFormat:@"%@",[[[resArr objectAtIndex:i] valueForKey:@"_source"] valueForKey:@"title"]];
             [self getMovie:movieName withElasticId:[NSString stringWithFormat:@"%@",[[[resArr objectAtIndex:i] valueForKey:@"_source"] valueForKey:@"id"]] completeBlock:^(NSDictionary *dict) {
                 if (dict!=nil) {
                     [newData addObject:dict];
                 }
                 
                 if ([newData count]==10) {
                     NSMutableDictionary *responseData = [[NSMutableDictionary alloc] init];
                     [responseData setObject:newData forKey:@"results"];
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                         completionBlock([self processResponseObject:responseData], nil);
                     });
                 }
             }];
         }

     }
         failure:^(NSURLSessionTask *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
     }];
}

#pragma mark - Data Parsing

- (NSArray *)processResponseObject:(NSDictionary*)data
{
    if (data)
    {
        NSArray* itemsList = [NSArray arrayWithArray:[data objectForKey:@"results"]];
        NSMutableArray* sortedArray = [[NSMutableArray alloc] init];
        
        for (NSDictionary* item in itemsList)
        {
            KMMovie* movie = [[KMMovie alloc] initWithDictionary:item];
            [sortedArray addObject:movie];
        }
        return sortedArray;
    }
    return nil;
}


#pragma mark - Private
- (NSString *)prepareUrlDetail:(NSString *)movieTitle
{
    if ([movieTitle rangeOfString:@", The"].location != NSNotFound) {
        NSLog(@"%@", [NSString stringWithFormat:@"%@",movieTitle]);
        
        NSArray *array = [movieTitle componentsSeparatedByString:@", The"];
        movieTitle = [NSString stringWithFormat:@"The %@",array[0]];
    }
    
    movieTitle = [movieTitle stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return [NSString stringWithFormat:kSimilarMoviesUrlFormat, [KMSourceConfig config].hostUrlString, [KMSourceConfig config].apiKey, movieTitle];
}

- (NSString *)prepareUrlElasticSearch
{
    return @"http://104.196.13.239:9200/bigmovie/film/_search?pretty";
}

- (void)getMovie:(NSString* )movieTitle withElasticId:(NSString* )movieElasticId completeBlock:(void ( ^ )(NSDictionary *dict))complete
{
    __block NSMutableDictionary *dict;
    AFHTTPSessionManager *managerSub = [AFHTTPSessionManager manager];
    NSString *url = [self prepareUrlDetail:movieTitle];

    NSLog(@"Request : %@", url);
    [managerSub GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObjectSub)
     {
         NSLog(@"JSON: %@", [responseObjectSub description]);
         if ([responseObjectSub valueForKey:@"results"]!=nil && [[responseObjectSub valueForKey:@"results"] count]>0) {
             dict = [[NSMutableDictionary alloc] initWithDictionary:[[responseObjectSub valueForKey:@"results"] objectAtIndex:0]];
             [dict setValue:movieElasticId forKey:@"elastic_id"];
             complete(dict);
         } else {
             dict = [[NSMutableDictionary alloc] initWithDictionary:responseObjectSub];
             [dict setValue:movieElasticId forKey:@"elastic_id"];
             complete(dict);
         }
     } failure:^(NSURLSessionTask *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
         complete(nil);
     }];
}

@end

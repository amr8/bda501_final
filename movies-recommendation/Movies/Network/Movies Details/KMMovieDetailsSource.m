//
//  KMMovieDetailsSource.m
//  TheMovieDB
//
//  Created by Kevin Mindeguia on 04/02/2014.
//  Copyright (c) 2014 iKode Ltd. All rights reserved.
//

#import "KMMovieDetailsSource.h"
#import "KMSourceConfig.h"
#import <AFNetworking/AFNetworking.h>
#import "KMMovie.h"

#define kSimilarMoviesUrlFormat @"%@?apikey=%@&t=%@"

@implementation KMMovieDetailsSource

#pragma mark - Init Methods

+ (KMMovieDetailsSource *)movieDetailsSource
{
    static dispatch_once_t onceToken;
    static KMMovieDetailsSource* instance = nil;
    
    dispatch_once(&onceToken, ^{
        instance = [[KMMovieDetailsSource alloc] init];
    });

    return instance;
}

#pragma mark - Request Methods

- (void)getMovieDetails:(NSString *)movieTitle movieElasticId:(NSString *)movieElasticId completion:(KMMovieDetailsCompletionBlock)completionBlock
{
    if (completionBlock)
    {        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
         [manager GET:[self prepareUrl:movieTitle] parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject)
         {
             NSLog(@"JSON: %@", responseObject);
             responseObject = [[NSMutableDictionary alloc] initWithDictionary:responseObject];
             [responseObject setValue:movieElasticId forKey:@"elastic_id"];
             dispatch_async(dispatch_get_main_queue(), ^{
                 [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                 completionBlock([self processResponseObject:responseObject], nil);
             });
         }
             failure:^(NSURLSessionTask *operation, NSError *error)
         {
             NSLog(@"Error: %@", error);
             dispatch_async(dispatch_get_main_queue(), ^{
                 [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

                 NSString* errorString = error.localizedDescription;

                 completionBlock(nil, errorString);
             });
         }];
    }
}

#pragma mark - Data Parsing

- (KMMovie *)processResponseObject:(NSDictionary*)data
{
    if (data)
    {
        return [[KMMovie alloc] initWithDictionary:data];
    }
    
    return nil;
}

#pragma mark - Private

- (NSString *)prepareUrl:(NSString *)movieTitle
{
    if ([movieTitle rangeOfString:@", The"].location != NSNotFound) {
        NSLog(@"%@", [NSString stringWithFormat:@"%@",movieTitle]);
        
        NSArray *array = [movieTitle componentsSeparatedByString:@", The"];
        movieTitle = [NSString stringWithFormat:@"The %@",array[0]];
    }
    
    movieTitle = [movieTitle stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return [NSString stringWithFormat:kSimilarMoviesUrlFormat, [KMSourceConfig config].hostUrlString, [KMSourceConfig config].apiKey,movieTitle];
}
- (NSString *)prepareElasticUrl:(NSString *)movieElasticId
{
    return [NSString stringWithFormat:@"http://104.196.13.239:9200/bigmovie/film/%@",movieElasticId];
}

@end

//
//  KMRelatedMoviesSource.m
//  TheMovieDB
//
//  Created by Kevin Mindeguia on 04/02/2014.
//  Copyright (c) 2014 iKode Ltd. All rights reserved.
//

#import "KMSimilarMoviesSource.h"
#import "KMSourceConfig.h"
#import "AFNetworking.h"
#import "KMMovie.h"

#define kSimilarMoviesUrlFormat @"%@?apikey=%@&t=%@"

@implementation KMSimilarMoviesSource

#pragma mark - Init Methods

+ (KMSimilarMoviesSource *)similarMoviesSource
{
    static dispatch_once_t onceToken;
    static KMSimilarMoviesSource* instance = nil;

    dispatch_once(&onceToken, ^{
        instance = [[KMSimilarMoviesSource alloc] init];
    });
    return instance;
}

#pragma mark - Request Methods
- (void)getSimilarElasticMovies:(NSString *)movieElasticId completion:(KMSimilarMoviesCompletionBlock)completionBlock{
    if (completionBlock)
    {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager GET:[self prepareElasticUrl:movieElasticId] parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject)
         {
             NSLog(@"JSON: %@", responseObject);
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                 
                 if (responseObject)
                 {
                     NSArray* itemsList = [NSArray arrayWithArray:[[responseObject objectForKey:@"_source"] objectForKey:@"indicators"] ];
                     NSMutableArray* sortedArray = [[NSMutableArray alloc] init];
                     
                     for (NSString* item in itemsList)
                     {
                         [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
                         
                         AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
                         [manager GET:[self prepareElasticUrl:item] parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject)
                          {
                              NSLog(@"JSON: %@", responseObject);
                              [self getMovie:[[responseObject objectForKey:@"_source"] valueForKey:@"title"] withElasticId:item completeBlock:^(NSDictionary *dict) {
                                  dict = [[NSMutableDictionary alloc] initWithDictionary:dict];
                                  [dict setValue:item forKey:@"elastic_id"];
                                  KMMovie* movie = [[KMMovie alloc] initWithDictionary:dict];
                                  [sortedArray addObject:movie];
                                  
                                  if ([sortedArray count]==[itemsList count]) {
                                      [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                                      completionBlock(sortedArray, nil);
                                  }
                                  
                              }];
                          }
                              failure:^(NSURLSessionTask *operation, NSError *error)
                          {
                              NSLog(@"Error: %@", error);
                          }];
                     }
                 }
             });
         }
             failure:^(NSURLSessionTask *operation, NSError *error)
         {
             NSLog(@"Error: %@", error);
             dispatch_async(dispatch_get_main_queue(), ^{
                 [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                 
                 NSString* errorString = error.localizedDescription;
                 
                 completionBlock(nil, errorString);
             });
         }];
    }

}

#pragma mark - Private
- (NSString *)prepareElasticUrl:(NSString *)movieElasticId
{
    return [NSString stringWithFormat:@"http://104.196.13.239:9200/bigmovie/film/%@",movieElasticId];
}

- (NSString *)prepareUrlDetail:(NSString *)movieTitle
{
    if ([movieTitle rangeOfString:@", The"].location != NSNotFound) {
        NSLog(@"%@", [NSString stringWithFormat:@"%@",movieTitle]);
        
        NSArray *array = [movieTitle componentsSeparatedByString:@", The"];
        movieTitle = [NSString stringWithFormat:@"The %@",array[0]];
    }
    
    movieTitle = [movieTitle stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return [NSString stringWithFormat:kSimilarMoviesUrlFormat, [KMSourceConfig config].hostUrlString, [KMSourceConfig config].apiKey,movieTitle];
}

- (void)getMovie:(NSString* )movieTitle withElasticId:(NSString* )movieElasticId completeBlock:(void ( ^ )(NSDictionary *dict))complete
{
    __block NSMutableDictionary *dict;
    AFHTTPSessionManager *managerSub = [AFHTTPSessionManager manager];
    NSString *url = [self prepareUrlDetail:movieTitle];
    NSLog(@"Request : %@", url);
    [managerSub GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObjectSub)
     {
         NSLog(@"JSON: %@", [responseObjectSub description]);
         if ([responseObjectSub valueForKey:@"results"]!=nil && [[responseObjectSub valueForKey:@"results"] count]>0) {
             dict = [[NSMutableDictionary alloc] initWithDictionary:[[responseObjectSub valueForKey:@"results"] objectAtIndex:0]];
             [dict setValue:movieElasticId forKey:@"elastic_id"];
             complete(dict);
         } else {
             complete(responseObjectSub);
         }
     } failure:^(NSURLSessionTask *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
         complete(nil);
     }];
}

@end
